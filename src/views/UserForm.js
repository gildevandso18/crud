import React, { useContext, useState } from 'react'
import { Text, View, TextInput, StyleSheet, Button} from 'react-native'
import UsersContext from '../context/UserContext'


export default ({route, navigation}) => {
    const [user, setUser] = useState(route.params ? route.params : {})
    const {dispatch} = useContext(UsersContext)
    return (
        <View style={styles.form}>
            <Text>Nome:</Text>
            <TextInput
                style={styles.imput}
                onChangeText={name => setUser({...user, name})}
                placeholder='Informe o Nome'
                value={user.name}
            />
            <Text>Email:</Text>
            <TextInput
                style={styles.imput}
                onChangeText={email => setUser({...user, email})}
                placeholder='Informe o Email'
                value={user.email}    
            />
            <Text>URL Avatar:</Text>
            <TextInput
                style={styles.imput}
                onChangeText={avatarUrl => setUser({...user, avatarUrl})}
                placeholder='Infome URL do Avatar'
                value={user.avatarUrl}
            />   
            <Button
                title='Salvar'
                onPress={() => {
                    dispatch({
                        type: user.id ? 'updateUser' : 'createUser',
                        payload: user,   
                    })
                    navigation.goBack()
                }}
            />

        </View>
    )
}

const styles = StyleSheet.create({
    form: {
        padding: 30
    },
    imput: {
        height: 40,
        fontSize: 20,
        paddingLeft: 5,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 10,
    }
})