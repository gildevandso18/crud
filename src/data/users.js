export default [
    {
        id: 1,
        name: 'Gildevan Santos',
        email: 'gil@empamil.com',
        avatarUrl:
            'https://cdn.pixabay.com/photo/2016/08/20/05/38/avatar-1606916__340.png'
    },
    {
        id: 2,
        name: 'Pedro Santos',
        email: 'pedro@empamil.com',
        avatarUrl:
            'https://cdn.pixabay.com/photo/2016/03/31/20/27/avatar-1295773__340.png'
    },
    {
        id: 3,
        name: 'Guego Santos',
        email: 'guego@empamil.com',
        avatarUrl:
            'https://cdn.pixabay.com/photo/2016/04/01/11/25/avatar-1300331__340.png'
    },
    {
        id: 4,
        name: 'Galego Santos',
        email: 'galego@empamil.com',
        avatarUrl:
            'https://cdn.pixabay.com/photo/2017/01/31/21/22/avatar-2027363_960_720.png'
    }
]